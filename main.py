import telegram
from requests import Response
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Updater, MessageHandler, Filters, CommandHandler, CallbackQueryHandler
from dotenv import load_dotenv
import os

from src.services.group_queue_service import GroupQueueService, EventTypes
from src.util import build_menu, log

load_dotenv()
BOT_KEY = os.getenv("TELEGRAM_BOT_KEY")
API_URL = os.getenv("API_URL")
queue_service = GroupQueueService(API_URL)


# @send_action(ChatAction.TYPING)
def handle_member_left_group(bot, update):
    if update.message.left_chat_member:
        user = update.message.left_chat_member
        print('member {member} left group'.format(member=user.full_name))
        chat_id = update.effective_chat.id
        response = queue_service.disable_member(chat_id, user.id)
        if response.ok:
            print(response.text)
            log(response.text, 'left')
            bot.send_message(chat_id, text="{name} foi removido da fila".format(name=user.full_name))
        else:
            print(response.text)
            log(response.text)


# @send_action(ChatAction.TYPING)
def handle_new_member(bot, update):
    print('new member')
    chat_id = update.effective_chat.id
    if len(update.message.new_chat_members) > 0:
        for new_user in update.message.new_chat_members:
            response = queue_service.register_new_user(chat_id, new_user, update.message.chat.title)
            if response.ok:
                bot.send_message(chat_id, text="{name} foi adicionado à fila".format(name=new_user.full_name))
            else:
                log(response.text)
    else:
        new_user = update.effective_user
        response = queue_service.register_new_user(chat_id, new_user, update.message.chat.title)
        if response.ok:
            bot.send_message(chat_id, text="{name} foi adicionado à fila".format(name=new_user.full_name))
        else:
            log(response.text)


def call_next(bot, chat_id):
    response: Response = queue_service.call_next(chat_id)
    if response.ok:
        data = response.json()
        current_user = data['member']
        if current_user['telegram_id'] == "":
            return bot.send_message(chat_id=chat_id, text="Nenhum membro disponível.")
        button_list = [
            InlineKeyboardButton("%s passou a vez" % current_user['first_name'],
                                 callback_data='%s,%s' % (EventTypes.SKIP.value, current_user['telegram_id'])),
            InlineKeyboardButton("%s não vem hoje" % current_user['first_name'],
                                 callback_data='%s,%s' % (EventTypes.ABSENT.value, current_user['telegram_id'])),
            InlineKeyboardButton("%s fez o café" % current_user['first_name'],
                                 callback_data='%s,%s' % (EventTypes.SUCCESS.value, current_user['telegram_id'])),
        ]
        reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=1))
        name = '[%s %s](tg://user?id=%s)' % (
            current_user['first_name'], current_user['last_name'], current_user['telegram_id'])
        text = "Barista da vez: {name}! (score: {score})\n" \
               "Selecione uma ação:".format(name=name, score=data['score'])
        bot.send_message(chat_id=chat_id, text=text, reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        log(response.text)


# @send_action(ChatAction.TYPING)
def handle_proximo_command(bot, update):
    print('próximo')
    call_next(bot, update.effective_chat.id)
    bot.delete_message(update.effective_chat.id, update.message.message_id)


# @send_action(ChatAction.TYPING)
def handle_user_response(bot, update):
    print('response event')
    chat_id = update.effective_chat.id
    user_response = update.callback_query.data
    current_user_id = user_response.split(',')[1]
    action = user_response.split(',')[0]

    response = queue_service.register_event(action, chat_id, current_user_id)
    if response.ok:
        data = response.json()
        member = data['membership']['member']
        responses = {
            EventTypes.SUCCESS.value: 'fez o café',
            EventTypes.ABSENT.value: 'não vem hoje',
            EventTypes.SKIP.value: 'passou a vez',
        }
        responder = update.effective_user.full_name
        name = '%s %s' % (member['first_name'], member['last_name'])
        score = data['membership']['score']
        action_msg = responses[action]
        message = f"{responder} disse que [{name}](tg://user?id={member['telegram_id']}) (score: {score}) {action_msg}"

        bot.send_message(chat_id, message, parse_mode=telegram.ParseMode.MARKDOWN)
        bot.delete_message(chat_id, update.callback_query.message.message_id)
        if action == (EventTypes.SKIP.value or EventTypes.ABSENT.value):
            handle_proximo_command(bot, update)


def handle_ranking_command(bot, update):
    print('ranking')
    chat_id = update.effective_chat.id
    response: Response = queue_service.get_ranking(chat_id)
    if response.ok:
        memberships = response.json()
        message = ''
        index = 0
        for membership in memberships:
            index = index + 1
            member = membership['member']
            name = member['first_name'] + ((' ' + member['last_name']) if member['last_name'] else '')
            score = membership['score']
            message += "{index} - {name} (score: {score})\n".format(index=index, name=name, score=score)
        bot.send_message(chat_id=chat_id, text=message)
    else:
        log(response.text)
        bot.send_message(chat_id=chat_id, text="Algo de errado não está certo!")


def handle_set_command(bot, update, args):
    print('set command')
    chat_id = update.effective_chat.id
    commands = {'+': 'SUCCESS', '-': 'DECREASE'}
    try:
        username = args[0][1:]
        cmd = args[1][0]

        if username and (cmd in commands.keys()):
            response = queue_service.register_event_by_username(chat_id, commands[cmd], username)
            if response.ok:
                data = response.json()
                member = data['membership']['member']
                nome = member['first_name']
                sobrenome = member['last_name'] if member['last_name'] else ''
                nome = "%s %s" % (nome, sobrenome)
                message = "{cmd}1 para [{nome}](mention:@{username})".format(nome=nome, username=username, cmd=cmd)
                bot.send_message(chat_id, message, parse_mode=telegram.ParseMode.MARKDOWN)
            else:
                print(response.text)

    except Exception as e:
        print(e)
        bot.send_message(chat_id=chat_id, text="exemplo: `/set @renatolb +`", parse_mode=telegram.ParseMode.MARKDOWN)


def main():
    print("Starting coffee group admin bot")
    # logging.basicConfig(level=logging.DEBUG,
    #                     format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    updater = Updater(BOT_KEY)
    dp = updater.dispatcher

    dp.add_handler(MessageHandler(Filters.status_update.new_chat_members, handle_new_member))
    dp.add_handler(MessageHandler(Filters.status_update.left_chat_member, handle_member_left_group))
    dp.add_handler(CommandHandler('register', handle_new_member))
    dp.add_handler(CommandHandler('ranking', handle_ranking_command))
    dp.add_handler(CommandHandler('proximo', handle_proximo_command))
    dp.add_handler(CommandHandler('set', handle_set_command, pass_args=True))
    dp.add_handler(CallbackQueryHandler(handle_user_response, pattern='^(SUCCESS,[0-9]+|SKIP,[0-9]+|ABSENT,[0-9]+)$'))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
