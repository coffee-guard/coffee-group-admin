FROM python:3.7 as server
ENV PYTHONUNBUFFERED 1
RUN mkdir /app
WORKDIR /app

RUN curl -sSL http://dides-git.tjro.net/snippets/32/raw | bash
ENV REQUESTS_CA_BUNDLE /etc/ssl/certs/ca-certificates.crt
ENV NODE_EXTRA_CA_CERTS /etc/ssl/certs/ca-certificates.crt

FROM server
COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt
COPY ./ /app

CMD python /app/src/main.py