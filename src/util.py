import hashlib
from datetime import datetime
from functools import wraps


def log(data, path):
    now = datetime.now()
    if not path:
        name = 'errors'
    logfile = open("logs/%s/%s%s%s%s%s%s.html" % (path, now.day, now.month, now.year, now.hour, now.minute, now.microsecond), "w+")
    logfile.write(data)
    logfile.close()


def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu


def send_action(action):
    """Sends `action` while processing func command."""

    def decorator(func):
        @wraps(func)
        def command_func(update, context, *args, **kwargs):
            context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
            return func(update, context, *args, **kwargs)

        return command_func

    return decorator
