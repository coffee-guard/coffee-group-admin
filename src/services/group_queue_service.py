from enum import Enum
from requests import Response
import requests


class EventTypes(Enum):
    SKIP = 'SKIP'
    ABSENT = 'ABSENT'
    SUCCESS = 'SUCCESS'


class GroupQueueService:
    def __init__(self, api_url):
        self.api_url = api_url

    def disable_member(self, chat_id, user_id):
        url = '{base}/membership/?group_telegram_id={group}&member_telegram_id={member}'.format(base=self.api_url,
                                                                                                group=chat_id,
                                                                                                member=user_id)
        print(url)
        return requests.delete(url)

    def register_new_user(self, chat_id, user, group_title):
        if not user.is_bot:
            post_data = {
                'group': {
                    'telegram_id': chat_id,
                    'title': group_title
                },
                'member': {
                    'telegram_id': user.id,
                    'first_name': user.first_name,
                    'last_name': user.last_name if user.last_name else '',
                    'username': user.username if user.username else ''
                },
            }
            print(post_data)
            return requests.post(self.api_url + '/membership/', json=post_data)

    def call_next(self, chat_id):
        return requests.get(self.api_url + '/group/{group_id}/get_next/'.format(group_id=chat_id))

    def register_event(self, action, chat_id, current_user_id):
        request_url = '/event/?group_telegram_id={chat_id}&member_telegram_id={user_id}'.format(chat_id=chat_id,
                                                                                                user_id=current_user_id)
        request_data = {'name': action}
        return requests.post(self.api_url + request_url, json=request_data)

    def register_event_by_username(self, chat_id, action, username):
        request_url = '/event/?group_telegram_id={chat_id}&member_username={username}'.format(chat_id=chat_id,
                                                                                              username=username)
        request_data = {'name': action}
        response = requests.post(self.api_url + request_url, json=request_data)
        return response

    def get_ranking(self, chat_id):
        return requests.get(self.api_url + '/group/{group_id}/ranking/'.format(group_id=chat_id))